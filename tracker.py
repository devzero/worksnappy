#!/usr/bin/env python3

import requests
import xml.etree.ElementTree as ET
import time
import os, os.path
import sys
import pickle
import calendar
from configparser import ConfigParser

calendar.setfirstweekday(calendar.MONDAY)
config = ConfigParser({ 'URL': 'https://api.worksnaps.com/api' })

configfn = os.path.expanduser('~/.trackerrc')
if os.path.exists(configfn):
    config.read([configfn])

def parse_dayspec(s):
    """ parses dayspec, list of n, n-m values; return set with matching days """
    days = set()
    for d in s.split(','):
        if '-' in d:
            (d1,d2) = d.split('-')
            d1 = int(d1.strip())
            d2 = int(d2.strip())
            while d1 <= d2:
                days.add(d1)
                d1 = d1 + 1
        else:
            days.add(int(d))
    return days

def parse_time(s):
    sign = 1
    if s[0] == '-':
        sign = -1
        s = s[1:]
    hhmm = s.split(':', 2)
    if len(hhmm) == 1:
        return int(s)
    return (int(hhmm[0])*60+int(hhmm[1]))*sign

def config_days(y, m):
    global config
    ret = { 'work': set(), 'rest': set() }
    for fmt in ['{}-{}', '{0}-{1:02d}']:
        section = fmt.format(y,m)
        if config.has_section(section):
            for opts in ret.keys():
                if config.has_option(section, opts):
                    ret[opts] = parse_dayspec(config.get(section, opts))
    return ret

def extraworktime(y, m):
    global config
    for fmt in ['{}-{}', '{0}-{1:02d}']:
        section = fmt.format(y,m)
        if config.has_section(section):
            if config.has_option(section, 'extrawork'):
                return parse_time(config.get(section, 'extrawork'))
    return 0

cachefn = os.path.expanduser('~/.tracker-cache')
cache = {}
if os.path.exists(cachefn):
    with open(cachefn, 'rb') as fp:
        cache = pickle.load(fp)

def workdays(y, m, rest = None, work = None, upto = None):
    c = calendar.Calendar()
    wdays = set()
    for d, w in c.itermonthdays2(y, m):
        if (upto is not None) and (d > upto):
            pass
        elif (d != 0) and (w < 5):
            wdays.add(d)
    if work is not None:
        for d in work:
            if (upto is None) or (d <= upto):
                wdays.add(d)
    if rest is not None:
        wdays = wdays - rest
    return wdays

def save_cache():
    global cache
    with open(cachefn, 'wb') as fp:
        pickle.dump(cache, fp)

def ymd_time(t = None):
    """ get current time as y, m, d list  """
    if t is None:
        ts = time.time()
    else:
        ts = t
    d = list(time.localtime(ts))
    return [ d[0], d[1], d[2] ]

def ymd_to_tm(ymd):
    """ convert y, m, d list into valid input for asctime/mktime """
    d = list(time.localtime())
    d[3] = 0
    d[4] = 0
    d[5] = 0
    for i in [ 0, 1, 2 ]:
        d[i] = ymd[i]
    return tuple(d)

def time_firstday(ymd = None):
    """ first day of month """
    if ymd is None:
        firstday = ymd_time()
    else:
        firstday = list(ymd)
    firstday[2] = 1
    return firstday

def time_nextday(ymd = None):
    """ first day of next day of month """
    if ymd is None:
        nextday = ymd_time()
    else:
        nextday = list(ymd)
    nextday[2] = 1
    if nextday[1] > 11:
        nextday[0] = nextday[0]+1
        nextday[1] = 1
    else:
        nextday[1] = nextday[1]+1
    return nextday

def time_regions(ymd = None):
    """ return pairs of start/end times for specified month """
    ret = []
    start = time_firstday(ymd)
    middle = list(start)
    middle[2] = 15
    end = time_nextday(ymd)
    # print("{}-{}-{} {}-{}-{} {}-{}-{}".format(start[2],start[1],start[0], middle[2], middle[1], middle[0], end[2], end[1], end[0]))
    return [
            [ time.mktime(ymd_to_tm(start)), time.mktime(ymd_to_tm(middle)) ],
            [ time.mktime(ymd_to_tm(middle)), time.mktime(ymd_to_tm(end)) ]
        ]

def hhmm(mins):
    """ render hh:mm """
    s = ''
    if mins < 0:
        s = '-'
        mins = (-mins)
    return '{}{}:{:02d}'.format(s,int(mins/60),int(mins%60))

headers = {
    'accept': 'application/xml',
    'content-type': 'application/xml',
}

if 'project_id' in cache:
    project_id = cache['project_id']
else:
    r = requests.get(config.get('global', 'URL') + '/projects.xml', headers=headers, auth=(config.get('global', 'API_TOKEN'), 'ignored'), verify=True)
    root = ET.fromstring(r.text)
    project_id = root[0][0].text
    cache['project_id'] = project_id
    save_cache()
    print('Project id is {}'.format(project_id))


if 'user_id' in cache:
    user_id = cache['user_id']
else:
    r = requests.get(config.get('global', 'URL') + '/me.xml', headers=headers, auth=(config.get('global', 'API_TOKEN'), 'ignored'), verify=True)
    root = ET.fromstring(r.text)
    user_id = root[0].text
    cache['user_id'] = user_id
    save_cache()
    print('User id is {}'.format(user_id))

now = ymd_time()
firstday = time_firstday(now)
nextday = time_nextday(now)

today_str = time.strftime('%Y%m%d', time.localtime())

date = None
days = set()
summary = 0
today = 0
def process_et(root):
    global summary
    global days
    global today
    for time_entry in root:
        for child in time_entry:
            if child.tag == 'from_timestamp':
                date = time.strftime('%Y%m%d', time.localtime(int(child.text)))
                days.add(date)
            if child.tag == 'duration_in_minutes':
                summary += int(child.text)
                if (date is not None) and (date == today_str):
                    today += int(child.text)

for times in time_regions():
    url = config.get('global', 'URL') + '/projects/'+str(project_id)+'/reports.xml?name=time_entries&from_timestamp='+str(times[0])+'&to_timestamp='+str(times[1])+'&user_ids='+user_id
    r = requests.get(url, headers=headers, auth=(config.get('global', 'API_TOKEN'), 'ignored'), verify=True)
    # print(r.text)
    process_et(ET.fromstring(r.text))

extramsg = ''
extrawork = extraworktime(now[0], now[1])
if extrawork != 0:
    extramsg = ' (real: {}, extra worktime: {})'.format(hhmm(summary), hhmm(extrawork))
    summary -= extrawork

print('Time spent: {}{}'.format(hhmm(summary), extramsg))

dayz = config_days(now[0], now[1])
wdays = workdays(now[0], now[1], upto=now[2], **dayz)

print('Active day(s): {}, work day(s): {}'.format(len(days), len(wdays)))

if today_str is not None:
    print('Worktime today: {}'.format(hhmm(today)))

baseline = len(wdays)*8*60
if summary >= baseline:
    print('Overtime: {}'.format(hhmm(summary-baseline)))
else:
    print('Undertime: {}'.format(hhmm(baseline-summary)))

all_days=len(workdays(now[0], now[1], **dayz))

print('Work day(s) this month: {}'.format(all_days))

# calculate with include and exclude current day
left_days=all_days-len(wdays)
left_days1=all_days-len(wdays)+1

mins_left = all_days*8*60-summary

print('Left to work: {}, day(s) {} or {}'.format(hhmm(mins_left), left_days1, left_days))
if(mins_left > 0):
    leftovers = []
    for days in [left_days1, left_days]:
        if days > 0:
            leftovers.append(hhmm(mins_left/days))
    print('Average work time to be on schedule: {}'.format(' or '.join(leftovers)))

